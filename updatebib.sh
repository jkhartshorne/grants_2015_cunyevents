#!/bin/sh

git -C ~/Dropbox/Documents/Resources/l3-bcbib pull
git -C ~/Dropbox/Documents/Resources/l3-bcbib commit -am "added citation"
git -C ~/Dropbox/Documents/Resources/l3-bcbib push

git submodule update --remote
