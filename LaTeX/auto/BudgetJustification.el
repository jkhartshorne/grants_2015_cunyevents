(TeX-add-style-hook
 "BudgetJustification"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "letterpaper" "margin=1in") ("titlesec" "compact") ("natbib" "sort") ("subfloat" "countmax") ("todonotes" "draft") ("caption" "font=small" "labelfont=bf" "skip=4pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "tocloft"
    "geometry"
    "graphics"
    "graphicx"
    "gensymb"
    "titlesec"
    "natbib"
    "xcolor"
    "array"
    "tikz"
    "bibentry"
    "booktabs"
    "ctable"
    "subfloat"
    "enumitem"
    "multirow"
    "breakurl"
    "wrapfig"
    "float"
    "hyperref"
    "lipsum"
    "amssymb"
    "caption"
    "subcaption"
    "todonotes"
    "pifont"
    "pbox")
   (TeX-add-symbols
    '("mycbox" 2)
    '("comment" 1)
    "squeezeup"
    "cmark"
    "xmark"
    "citeapos"
    "patchsect")))

