(TeX-add-style-hook
 "summary"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "letterpaper" "margin=1in") ("titlesec" "compact") ("natbib" "sort") ("subfloat" "countmax") ("todonotes" "disable") ("caption" "font=small" "labelfont=bf" "skip=4pt")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "tocloft"
    "geometry"
    "graphics"
    "graphicx"
    "gensymb"
    "titlesec"
    "natbib"
    "xcolor"
    "array"
    "tikz"
    "bibentry"
    "booktabs"
    "ctable"
    "subfloat"
    "enumitem"
    "multirow"
    "breakurl"
    "wrapfig"
    "float"
    "hyperref"
    "lipsum"
    "amssymb"
    "caption"
    "subcaption"
    "todonotes"
    "pifont"
    "pbox")
   (TeX-add-symbols
    '("mycbox" 2)
    '("comment" 1)
    "squeezeup"
    "cmark"
    "xmark"
    "citeapos"
    "patchsect")))

