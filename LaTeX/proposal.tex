%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

\documentclass[11pt]{article}

\usepackage{tocloft}
\usepackage[letterpaper,margin=1in]{geometry} %used for margins
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{gensymb} %for symbols such as the degree sign
\usepackage[compact]{titlesec} %shrink margins around section headings
\graphicspath{{figures/}{../figures/}}
% \usepackage[natbibapa]{apacite}
\usepackage[sort]{natbib}
\usepackage{xcolor}
\usepackage{array}% http://ctan.org/pkg/array
\usepackage{tikz} %used for drawing colored boxes
\usepackage{bibentry} %for full citations
% \nobibliography*
%\usepackage{subfig} %for creating panels %Tobi uses, but conflicts
%with subcaption package
\usepackage{booktabs}% http://ctan.org/pkg/booktabs
\usepackage{ctable}% http://ctan.org/pkg/booktabs
\usepackage[countmax]{subfloat} %for creating panels
\usepackage{enumitem} %better environment for lists
\usepackage{multirow} %to have multiple row entries in tables
\usepackage{breakurl} %to break urls
\usepackage{wrapfig} %to wrap figures
\usepackage{float} %to h ave links within the document
\usepackage{hyperref} %to h ave links within the document
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}

\usepackage{lipsum}
\usepackage{amssymb} %some extra math symbols
\usepackage{caption} %to get captions inside minifigures
\usepackage{subcaption} %for getting captions for subfigures

\usepackage[disable]{todonotes} % notes not showed
% \usepackage[draft]{todonotes}   % notes showed

% Select what to do with command \comment:
\newcommand{\comment}[1]{}  %comment not showed
%\newcommand{\comment}[1]
%{\par {\bfseries \color{blue} #1 \par}} %comment showed

% OK You're going to need to tell me how this part gets used

% \commenting true
% \fi

% \ifcommenting
% \newcounter{note}
% \stepcounter{note}
% \newcommand{\n}[1]{\textcolor{red}{$^{\mathrm{n}}$\footnotemark[\arabic{note}]}\footnotetext[\arabic{note}]{\textcolor{red}{NOTE:
%       #1}}\stepcounter{note}}
% \newcommand{\q}[1]{\textcolor{green}{[QUESTION: #1]}}
% \newcommand{\td}[1]{\textcolor{blue}{[TODO: #1]}}
% \newcommand{\del}[1]{\textcolor{yellow}{[DELETED: #1]}}
% \else
% \newcommand{\n}[1]{}
% \newcommand{\q}[1]{}
% \newcommand{\td}[1]{}
% \newcommand{\del}[1]{}
% \fi

%to allow for more figures per page
% \renewcommand\floatpagefraction{.95}
% \renewcommand\topfraction{.95}
% \renewcommand\bottomfraction{.95}
% \renewcommand\textfraction{.05}
% \setcounter{totalnumber}{50}
% \setcounter{topnumber}{50}
% \setcounter{bottomnumber}{50}

%some settings to make lists look nicer
\setlist{leftmargin=*}
\setlist[1]{labelindent=\parindent} % Only the level 1
\setlist{topsep = 0cm,partopsep = 1pt, parsep = 1pt} %changes the separation of lists

%some stuff to decrease the space around section headings, etc.
\titleformat{\section}
  {\bfseries \normalsize}{\thesection}{0.5em}{}

\titleformat{\subsection}
  {\bfseries \normalsize}{\thesubsection}{0.5em}{}


  \titleformat{\subsubsection}
{\bfseries \normalsize}{\thesubsubsection}{0.5em}{}

\titlespacing{\section}{0cm}{-.05cm}{-.15cm}
\titlespacing{\subsection}{0cm}{-.05cm}{-.15cm}
\titlespacing{\subsubsection}{0cm}{-.05cm}{-.15cm}

%decreasing space around figures and tables
\setlength{\textfloatsep}{10pt}
\setlength{\intextsep}{10pt}

%setting font size for captions
\usepackage[font=small,labelfont=bf,skip=4pt]{caption}
\captionsetup[table]{font=small,skip=4pt}

%change footnote font size to comply with NSF regulations
\renewcommand{\footnotesize}{\normalsize}

%command to create boxes
\newcommand{\mycbox}[2]{\tikz{\path[draw=#1,fill=#2] (0,0) rectangle (1cm,1cm);}}

%for tick marks
\usepackage{pifont}% http://ctan.org/pkg/pifont
  \newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

%for possessive citing
\def\citeapos#1{\citeauthor{#1}'s (\citeyear{#1})}

%a little space between paragraphs
\setlength{\parskip}{1em}

%set fonts
%\usepackage[scaled]{helvet}
\renewcommand\familydefault{phv}
%\usepackage[T1]{fontenc}

\usepackage{pbox}

%\pagenumbering{gobble}

\makeatletter
\def\patchsect#1\let\@svsec\@empty{#1\def\@svsec{\leavevmode\kern1sp\relax}}
\let\old@sect\@sect
\def\@sect{\expandafter\patchsect\old@sect}
\makeatother

\begin{document}

\noindent\textbf{Proposal for the Workshop on Events in Language and Cognition
  2016.}

\noindent \textbf{Chairperson:}\\\noindent\textit{Joshua K. Hartshorne}, Assistant
Professor, Department of Psychology, Boston College

\noindent \textbf{Additional Organizers:}\\\noindent\textit{Melissa Kline}, Post-Doctoral
Fellow, Department of Psychology, Harvard
University\\\noindent\textit{Eva Wittenberg}, Post-Doctoral Fellow,
Department of Psychology, University of California-San Diego\\

\vspace{20pt}

\section{Motivation for the Workshop}
\label{sec:Motivation}

\subsection{Language and Events}

\noindent The psychological representation of events is fundamental to
broad swaths of the language sciences. Most obviously, we talk about
events (\textit{Yesterday, it was raining, so I took my umbrella, but
  it got broken by the wind and I threw it out and bought a new
  one}). Thus, any theory of communication ultimately needs to account for how events map onto
language. Likewise, the influence of language on event representations
(and vice versa) has long been a central concern in
psycholinguistics, particularly regarding differences in
interpretation that result from different languages or different ways
of describing something within a single language
\citep{Brown:1983p3922,Wittenberg:2014aa,Fausey:2010aa}. The language-event interface has
lately received particularly intense focus in the applied fields of
natural language processing and robotics, where natural language
interfaces have become increasingly important in designing planning systems; for example, one would like a robot in a warehouse to understand simple sentences such as \textit{Take this parcel and put it in the red truck to the left of
  the large box} \citep{Tellex:2014aa}.

\noindent Event representations are also implicated in language in less obvious but
no less fundamental ways. A diverse group of language acquisition
researchers, representing a number of competing research traditions,
have argued that infants make extensive use of event representations
when acquiring language
\citep{ambridge2013retreat,Pinker:1989wr,gleitman1990structural,pinker1984language,Goldberg:CaW2006}. For
instance, Syntactic Bootstrapping \citep{gleitman1990structural}
proposes that children use expectations about how linguistic structure
and event structure are related in order narrow down the space of
possible meanings for new words, thereby solving Quine's
(\citeyear{Quine1960wao}) Paradox \citep{Medina2011,Fisher2010}. In fact, although the exact mechanics remain a
point of contention, there is broad agreement among linguists
that aspects of event meaning are intimately involved in determining verb
argument structure and subcategorization rules (e.g., why
\textit{Archibald amused Beatrice} is grammatical but
\textit{Archibald laughed Beatrice} is not) \citep{Jackendoff:1990vu,Goldberg2003,Dowty:1991p6873,FillmoreCaseForCase}. Finally,
researchers working at the level of discourse understanding have
argued that the structure of discourse is tied to cognitive representations of the relations among events \citep[e.g.,][]{Kehler:2002p5022,Kaiser:2012aa}.

\subsection{The need for a workshop}

\noindent In principle, the broad importance of encoding events in language presents two related
opportunities. First, the need for our theories of events to explain multiple
phenomena (in language and in nonlinguistic cognition) sharply
constrains those theories.  Second, the things we learn about events
and their relationship to language in one domain (e.g. motion events)
can inform research in another domain (e.g. caused changes-of-state) \cite{Levin:2013aa}.
 Moreover, theoretically rich models of event
representations are important for related fields such as cognitive
psychology, social psychology, and artificial intelligence, and so cross-pollination
should be both possible and fruitful.

\noindent It is perhaps a surprise, then, that the various roles of
event representations in language -- and cognition more broadly -- are
studied largely independent of one another. With at most a few
exceptions \citep[e.g.,][]{pinker.s:2007}, reviews of ''event
cognition'' or ''events in language'' tend to focus on one or at most
two of the many events literatures
\citep{Hovav:2005p8491,Radvansky:2014aa,Papafragou2015events}. Conferences
and workshops that address events from a broad perspective are few and
far between (see sec. \ref{ssec:Previous}).

\noindent This lack of integration significantly slows the pace of
research. For instance, there is a 40-year literature on the effect of
event-type on explanation \citep{Garvey:1974p9012,Brown:1983p3922,Rudolph:1997p2753}. Specifically, the choice of verb
systematically affects the types of explanations people expect:
Listeners prefer \textit{Archibald liked Beatrice
  because...} to continue with a reference to Beatrice, but expect
\textit{Archibald surprised Beatrice because...} to refer to
Archibald. Despite decades of work, researchers were unable to explain
or predict the effects of different verbs. Recently, Hartshorne and Snedeker
(\citeyear{Hartshorne:hi}) showed that a two decade-old theory in the
argument structure literature \citep{Levin:1993uf} explained the explanation data very
well. This result -- which has implications for explanations, pronoun
interpretation, and verb argument structure \citep[cf.][]{Hartshorne:hi}
-- may well have been discovered much earlier, except that
research into explanations and verb argument structure had proceeded
completely independently, despite both being deeply concerned with how
verbs encode the causes of events.

\noindent Thus, the central goal of the workshop is to bring together
researchers who study different aspects of the interface between language and event
structure, in order to cross-pollinate research programs and advance
the language sciences. A longer-term goal is for the workshop to help
stimulate the development of a coherent research community and
research literature concerned with events.

\subsection{Timeliness}
\label{ssec:Timeliness}

\noindent Given the importance of event representations for the
language sciences, there is not really a wrong time for a scientific
meeting on that topic, particularly given the dearth of such
meetings.

\noindent However, there are compelling reasons for having such a
meeting now in particular. Recent years have seen rapid growth in the
broadly-construed events literature, both within the language sciences
and without. Within the language sciences, there is a rapidly-growing
body of work on the role of event representations in language
acquisition \citep{Papafragou2015events,hartshorne2015love,ambridge2013retreat},
argument structure \citep{Wittenberg:2014aa,Hovav:2005p8491,Croft:2012ti}, online sentence processing \citep{Arnold:2013aa,Kaiser:2012aa}, and the
role of event semantics in Natural Language Processing
\citep{Ikuta:2014aa,Tellex:2014aa}.

\noindent Beyond the language sciences, several highly relevant
literatures have developed. Over the past twenty years, researchers
have uncovered a great deal about how very young children
perceive and reason about events
\citep{Spelke:2007ib,Wu:2015aa}. The fact that 12-month-olds who are
just beginning to learn language may already have access to abstract
non-linguistic (but linguistically relevant) concepts like animacy \citep{Luo:2009aa} opens up a range of possible acquisition stories and plausible targets for connections between language and event representations. Another large literature has grown up around the question of
how adults segment
continuous streams of visual information into discrete events
\citep{Radvansky:2014aa}, and how machines can be taught to segment and
identify events in videos \citep{Bhattacharya:2014aa}. Identifying salient ''chunks'' of time for event analysis may be a critical step in event perception, providing clues to the important scene information we use to construct cognitive representations of events.

\noindent These trends present an
opportunity for the broader language community, both to learn from
these findings but also to make sure that these other communities
benefit from what has been learned about events by language
scientists. For instance, it has not gone entirely unnoticed that many of the
representations uncovered in the infant cognition literature, such as
AGENT and PATH, have also figured heavily in
many theories of argument structure
\citep[cf.][]{Lakusta:2012wc}. Likewise, there has been some
interplay between the event segmentation literature in psychology and
the discourse literature within the language sciences \citep{Radvansky:2014aa}. However, the vast majority of event
constructs proposed in the language literature have never been
investigated in these affiliate literatures, and many of the basic
assumptions of the different literatures are in direct opposition. For
instance, a driving concern of the language acquisition
literature has been the ''multiple construal problem'' -- the fact
that the same scene might be construed in different ways as different types of events
(see Figures \ref{fig:Catch} -- \ref{fig:Break}) \citep{gleitman1990structural,Gentner:2001p9645}. In contrast, the
event segmentation and event labeling literatures have focused on
identifying \textbf{the} single action taking place in a scene.

\begin{wrapfigure}{R}{0.6\textwidth}
\begin{minipage}[t]{.275\textwidth}
%  \centering
  \includegraphics[width=\linewidth]{Catch}
  \captionof{figure}{An example of throwing, giving, receiving, and catching.}
  \label{fig:Catch}
\end{minipage}%
\begin{minipage}[t]{0.05\textwidth}
   \includegraphics[width=\linewidth]{whitespace}
%nothing to see here. Just spacing.
\end{minipage}%
\begin{minipage}[t]{.275\textwidth}
%  \centering
  \includegraphics[width=\linewidth]{Break}
  \captionof{figure}{An example of breaking, crushing, squeezing, and holding.}
  \label{fig:Break}
\end{minipage}
\end{wrapfigure}

\noindent The limited interaction between the different event
literatures represents a lost opportunity, both for the language
sciences and for the other sciences. It is not surprising that the
event representations posited within the language sciences are far
richer than those posited in psychology or artificial intelligence. Language provides unparalleled (if indirect) insight into
cognitive representations \citep{pinker.s:2007}. Much of the mind (including event
representations) is opaque to direct observation, and so cognitive
psychologists are forced to design semi-unnatural tasks in order to
find out about the nature of conceptual representations. Language, in
contrast, is externalized: It is a highly-structured signal that
necessarily reflects the underlying structure of the mind. Not
surprisingly, whereas event researchers outside of language have
focused on relatively high-level, unstructured representations of
events (e.g., event segmentation), language scientists have proposed -- and provided evidence
for -- rich, compositional representations of events. Given that,
for the progress of science as a whole, it would be ideal for event
researchers outside of the language sciences to be constrained by the
results, phenomena, and theoretical progress of the language
sciences (and, of course, \textit{vice versa}). One purpose of the Workshop is to help facilitate such exchanges.

\subsection{Goals}
\label{ssec:Goals}

\noindent By bringing together researchers who work on different
aspects of the the interface between language and event structure, the
Workshop will facilitate the exchange of ideas, findings, and
insights, furthering a cumulative science of language. Live meetings
such as this one are particularly important in the absence of
comprehensive published reviews. We hope that
the Workshop, in conjunction with previous and future events (see
Table \ref{t:prev}), will help stimulate the development of a coherent
research community and literature. Moreover, a more coherent community around
events and language would be in a better position to contribute to and
benefit from the burgeoning communities in affiliated fields, such as cognitive psychology and
robotics (see sec. \ref{ssec:Timeliness}).

\noindent In order to facilitate these goals, we have asked presenters
to focus on three broad questions:

\begin{itemize}
\item How do children and adults mentally represent events at the
level of abstraction that is encoded in language?
\item How is the structure of events represented in and conveyed by
language?
\item How to the constraints of the infant cognitive system and
questions of learnability constrain our theories of language – and how
does language constrain our theories of the infant cognitive system?
\end{itemize}

\noindent These questions will also serve to focus the discussion
session at the end of the Workshop (see sec. \ref{sec:Organization}).

\subsection{Previous conferences and workshops}
\label{ssec:Previous}

\noindent Recent years have seen a renewed interest in event structure
as it relates to language, which is reflected in the recent workshops on that topic (Table \ref{t:prev}). However, most have focused on
specific issues, such as argument structure. To the organizers' knowledge, the only recent
conference or symposium to focus broadly on events was the
Interdisciplinary Research Symposium on Events in 2012.

\begin{table}[]
\centering
{\small
\caption{Previous conferences on event structure and language}
\label{t:prev}
\begin{tabular}{lll}
\hline
3/31/2012       & University of Maryland         & Interdisciplinary Research Symposium on Events                                                                                           \\
9/1/2011        & CNRS Paris                     & Structuring the Argument                                            \\
3/17/2010       & Universitat Pompeu Fabra       & Workshop on the subatomic semantics of event predicates                                                                                               \\
7/4/06-7/7/06   & Hebrew University of Jerusalem & Workshop on syntax, lexicon, and event structure                                                                                                              \\
6/26/97-6/27/97 & Cornell                        & Workshop on Events as Grammatical Objects                                                                                                                       \\
\hline
\end{tabular}}
\end{table}

\section{Invited Speakers}
\label{sec:InvitedSpeakers}

\noindent Two leading scientists in psycholinguistics have enthusiastically agreed to give talks and to participate in the workshop. Brief descriptions of the invited speakers’ research
programs are provided below, along with recent examples of their research as
well as some of their seminal work.

\noindent\textbf{Jesse Snedeker}. Jesse Snedeker is a psychologist and
psycholinguist studying semantic representations and their relation to
syntax and pragmatics.  She approaches these questions by studying the
construction of semantics during real time language comprehension and
over the course of language acquisition.  The study of semantics has
been based largely on the judgments of trained linguists.  Where these
judgments are unclear, controversial or uninformative, theories
diverge.  Snedeker's approach - studying moment-to-moment language
comprehension - allows for additional insight into the processes that
give rise to meaning and the representations they create.  She focuses
primarily on developmental work that gets at the fundamental
architecture of language and explores the relation between language
development and conceptual development.  For instance, her work on
understanding how children's expectations about transitive sentences
converge and depart from adult semantics yield insights into the
contributions that the child's own conceptual biases make to language
acquisition.

\noindent\textit{Representative publications}:

\noindent Hartshorne, J. K. \& Snedeker, J. (2013). Verb argument structure predicts implicit causality: The advantages of finer-grained semantics. \textit{Language and Cognitive Processes}, 28(10), 1474-1508.

\noindent Hartshorne, J. K., Pogue, A., \& Snedeker, J. (2015). Love is hard to understand: The relationship between
transitivity and caused events in the acquisition of emotion
verbs. \textit{Journal of Child Language}, 42, 467-504.

\noindent Barner, D., Wagner, L., \& Snedeker, J. (2008). Events and
the ontology of individuals: Verbs as a source of individuating mass
and count nouns. \textit{Cognition}, 106, 805-832.

\noindent Wittenberg, E., \& Snedeker, J. (2014). It takes two to
kiss, but does it take three to give a kiss? Categorization based on
thematic roles. \textit{Language, Cognition and Neuroscience}, 29.5,
635-641.

\noindent Yuan, S., Fisher, C. \& Snedeker, J. (2012). Counting the nouns: Simple structural cues to verb meaning. \textit{Child Development}, 83 (4), 1382â€“1399.

\noindent\textbf{Elsi Kaiser}. Elsi Kaiser is a psycholinguist
studying (i) how different kinds of information interact and are
integrated during language processing and (ii) what this can tell us
about the nature of the mental representations activated during
processing. She approaches these themes primarily from the perspective
of reference resolution, especially the comprehension of different
kinds of referential forms across languages such as pronouns (e.g.,
she), reflexives (e.g., himself) and demonstratives (e.g., this). This
work reveals the structured models of events that we use to comprehend
language, tracking the actors as they participate in related actions
over time. In particular, she has shown that these models overlap to
some extent with motor representations - priming participants by
having them perform either causal or noncausal actions influences
their interpretation of discourses by changing their expectation of
how subsequent events described in language will relate to one
another. These kinds of integrated processing between language and
cognitive processing are some of the challenges that a novel theory of
event structure - spanning language, conceptual representation and
even motor planning - may be able to provide.

\noindent\textit{Representative publications}:

\noindent Kaiser, E. (2012) Taking action: a cross-modal investigation of discourse-level representations. \textit{Frontiers in Psychology} 3:156.

\noindent Wu, F., Kaiser, E. \& Andersen, E.. (2012). Animacy
Effects in Chinese Relative Clause Processing. \textit{Language and
  Cognitive Processes}, 27(10), 1489-1524.

\noindent Kaiser, E. (2011). Salience and contrast effects in reference resolution: The interpretation of Dutch pronouns and demonstratives, \textit{Language and Cognitive Processes}, 26, 1587-1624

\noindent Kaiser, E., Runner, J. T., Sussman, R. S. \& Tanenhaus,
M. K. (2009). Structural and semantic constraints on the
resolution of pronouns and reflexives. \textit{Cognition}, 112, 55-80

\section{Workshop Organization}
\label{sec:Organization}

\noindent The workshop will consist of two invited talks,
nine juried talks, and a concluding discussion
panel. As discussed below (see sec. \ref{sssec:Students}), if there
are enough quality submissions, two of the slots for juried talks will
be converted into a 1-hour poster session. The discussion panel will
begin with 20-minute commentaries by each of our Distinguished
Commentators (see below), followed by free Q\&A involving all of the
oral presenters.

\noindent In addition, in order to facilitate conversation, there will
be two coffee breaks and a lunch break. The organizers will also be
hosting a ''speakers' dinner'' (no funding is requested for the dinner).

\subsection{Location and Venue}
\label{ssec:Location}

\noindent The Workshop will be co-located with the 29th Annual CUNY
Conference on Human Sentence Processing
(cuny2016.lin.ufl.edu/), hosted by the the University of Florida in
Gainesville, Florida. The workshop will take place on March 2, 2016 --
the day prior to the start of the three-day CUNY conference. The CUNY
Sentence Processing Conference was founded in 1988 at the City
University of New York. The conference was a success and became an
annual event, retaining the name of its founding institution although
other universities have also hosted many of its meetings since its
first â€˜away dateâ€™ in 1992. The conference has been held annually for
over a quarter century, without administrative support from any formal
organization.

\noindent Organizing the Workshop in conjunction with the CUNY
Conference is ideal, because the CUNY Conference is the premier conference in the world for
scientists interested in how humans process higher levels of
language. Each year, approximately 350-400 scientists (faculty,
postdoctoral fellows and graduate students) attend the conference as
presenters and audience members. Conference attendees come from all over the United States, from Europe and from East Asia, and from disciplines as diverse as psychology, linguistics, neuroscience, computer science and philosophy. Thus, there is significant overlap between our target audience and the attendees of the CUNY Conference, which both decreases the burden on attendees and increases the likelihood interested researchers will hear about the Workshop. Note that the Workshop is being advertised both on the CUNY Conference website and through the CUNY Conference listserv (see Section \ref{ssec:Promotion}).

\noindent The Workshop will take place at the Hilton Garden Inn in Gainseville, FL. This venue is within a walking distance (or very short car trip) of the CUNY conference location.

\section{Presenters and Participants}
\label{sec:PresentersAndParticipants}

\subsection{Recruiting Participants and Presenters}
\label{ssec:Promotion}

\noindent The Workshop has been advertised on the CUNY Conference
website and through the CUNY, AMLaP, CHILDES, CogDev, and LinguistList
listservs. In addition, the organizers -- who know many of the
researchers in the field personally -- have been working through their
extended networks in order to ensure that potentially interested
researchers are aware of the Workshop, and have received very positive feedback.

\noindent While the focus of this workshop is what can be learned
about event representations through the study of language (and vice
versa), and so our audience is primarily psycholinguists, the nature
of the research is that it is inherently interdisciplinary. In order
to build bridges to event researchers in other fields (e.g. theoretical linguistics, robotics, event perception), we plan to
invite two Distinguished Commentators from among these fields
(e.g., Jeffrey Zacks, Barbara Tversky, Brian Scholl, or Beth
Levin). The role of the Distinguished Commentators is to serve as
conduits between fields, informing psycholinguists about what has been
learned in their own field, and taking some of what psycholinguists
have learned back into their own field. Formally, this will be
accomplished through their commentary on the prior talks during the
final session of the Workshop. Informally, we hope their presence
will spur dialog both during breaks and Q\&As as well as
subsequent to the Workshop.

\subsection{Selection of Presentations}

\noindent The two invited speakers were chosen by
committee. The remaining presenters will be chosen on the basis of
two-page abstracts. The organizers will solicit reviews from experts
in psycholinguistics and event representation. Because the explicit goal of this workshop
is to build a coherent research field, the organizers will choose
presenters based on these reviews with the constraint that the final
roster should represent a diversity of perspectives, approaches, and
researchers (see also below). Abstracts submitted by the three co-organizers will be rated by others in the review pool, and will be selected for no more than two of the nine juried talks (or no more than one talk in the case that a poster session is created.)

\subsubsection{Student Participation}
\label{sssec:Students}

\noindent An central goal of the Workshop is to catalyze the growth of
event representation as a coherent field of study. Thus, the Workshop
organizers are highly motivated to encourage student participation
(note that the two co-organizers are both themselves
post-docs). Depending on the number of submissions, there will be a
poster session in order to ensure that students have the opportunity
to present. Moreover, in selecting spoken presentations, the
organizers will take diversity of seniority into consideration.

\subsection{Promotion of diversity}
\label{ssec:Diversity}

\noindent An explicit aim of the Workshop is to build a coherent field
of event cognition within the language sciences community. Thus, the
organizers are strongly committed to a diverse and representative body
of speakers and audience members. Our interest in diversity extends to
both research topic and approach (generative and constructivist
approaches to language, adult psycholinguistics and language
acquisition, theories and models and experimental data, etc.) as well
as demographics (gender, seniority, etc.).

\noindent To achieve diversity in the audience, we have advertised the
Workshop broadly (see sec. \ref{ssec:Promotion}). Diversity will be
taken into consideration in choosing presentations -- both spoken
presentations and posters (if any).

\subsection{Childcare}
\label{ssec:Childcare}

\noindent Given the small size of the workshop. we do not have a formal
program to accommodate family-related needs. However, it is not
unusual for participants at similar workshops to bring their children
into sessions, whether they are infants, toddlers, or teenagers
(without, of course, paying the registration fees!). Upon registration,
we will provide an option to sign up to an email group that will allow
attendees to coordinate childcare arrangements among themselves to
reduce costs. If any participant has any family-related needs beyond
this type of flexibility, the organizers will accommodate them.

\section{Dissemination of results}

\noindent Diversity in attendees (see sec. \ref{ssec:Diversity}) will
help ensure that word-of-mouth is a viable mechanism for dissemination
of results. Presenters will also be encouraged to submit their slides
for posting on the workshop website.

\clearpage
% \bibliography{/Users/josh/Dropbox/Documents/PapersLibrary/FullLibrary.bib}
\bibliography{../l3-bcbib/LLLL.bib}
% \bibliographystyle{apacite}
%\bibliographystyle{naturemag}
\bibliographystyle{apalike}

\listoftodos
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
